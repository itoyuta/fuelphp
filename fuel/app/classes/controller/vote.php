<?php

class Controller_Vote extends Controller{
	
	
	
	public function action_login(){
		
		if(Input::post("id") != "" ){
			if(Auth::login( Input::post("id"),Input::post("password") ) ){
				Response::redirect("vote/view");
			}
		}
	
		
		/*
		if( Input::post("id") == "admin" && Input::post("password") == "pass" ){
			Session::set("admin_login", true );
			echo "ログインOK";
		}
		if( Session::get("admin_login") == true ){
			Response::redirect("admin/view");
		}
		*/
		
		return
		Response::forge( View::forge("vote/login") );
		
		
	}
	
	public function action_view(){
		
		
		if(! Auth::check()){
			Response::redirect("vote/login");
		}
		
		
		$images = Model_Image::find("all");
		$data = array("images" => $images );
		
		return
		Response::forge(View::forge("vote/view", $data ));
		
		
	}
	
	public function action_logout(){
		
		Auth::logout();
		Response::redirect("vote/login");
		
		
		/*
		Session::delete("admin_login");
		Response::redirect("admin/login");
		*/
		
	}
	
	/*
	public function action_upload(){
		if( Session::get("admin_login") != true ){
			Response::redirect("admin/login");
		}
		
		if( Input::file("image") ){
			$image = Input::file("image");
			move_uploaded_file($image["tmp_name"], "assets/img/". $image["name"]);
			
			$model_image = new Model_Image();
			$model_image->file_name = $image["name"];
			$model_image->info = "";
			$model_image->votes = 0;
			$model_image->save();
			
		}
		
		
		return
		Response::forge(View::forge("admin/upload"));
	}
	*/
	
}
