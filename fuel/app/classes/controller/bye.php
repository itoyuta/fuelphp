<?php

class Controller_Bye extends Controller{
	
	public function action_index(){
		$data = array(
			"title" => "わかめの挨拶",
			"message" => "ばいばいや",
		);
		$view = View::forge("bye/index", $data);
		return Response::forge($view);
	}
}
