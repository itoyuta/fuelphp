<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>アップロード</title>
</head>

<body>
アップロード画面
<?php echo Html::anchor("admin/logout", "ログアウト"); ?>
<br>
<?php echo Html::anchor("admin/view", "いちらん"); ?>

<?php echo Form::open( array( "action" => "admin/upload",
					 		  "method" => "post",
							  "enctype" => "multipart/form-data"));
?>
<?php echo Form::file("image"); ?>
<?php echo Form::submit("upload", "アップロード"); ?>
<?php echo Form::close(); ?>
</body>
</html>